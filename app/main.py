import uvicorn
from app import config
from app.note import controller


app = config.app

app.include_router(controller.router, tags=['Notes'], prefix='/api/notes')


@app.get("/api/")
def root():
    return {"message": "Welcome to FastAPI with SQLAlchemy"}

