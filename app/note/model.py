from app.database import Base
from sqlalchemy import TIMESTAMP, Column, String, Boolean, Integer
from sqlalchemy.sql import func
from fastapi_utils.guid_type import GUID, GUID_DEFAULT_SQLITE


class Note(Base):
    __tablename__ = 'notes'
    id = Column(Integer, primary_key=True, index=True) #Column(GUID, primary_key=True, default=GUID_DEFAULT_SQLITE)
    title = Column(String(150))
    content = Column(String(150))
    category = Column(String(150))
    published = Column(Boolean, default=True)
    createdAt = Column(TIMESTAMP(timezone=True),
                       nullable=False, server_default=func.now())
    updatedAt = Column(TIMESTAMP(timezone=True),
                       default=None, onupdate=func.now())
