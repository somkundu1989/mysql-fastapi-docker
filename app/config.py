import os
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

load_dotenv()

app_title = os.getenv("APP_TITLE")
app = FastAPI(title=app_title)

origins = [
    "http://"+os.getenv("APP_HOST")+":"+os.getenv("APP_PORT"),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

database_url = os.getenv("DATABASE_URL")

# will create db tables
# from app.note import model
# from app.database import engine
# model.Base.metadata.create_all(bind=engine)